import torch
import torch.nn as nn
class Diverter(nn.Module):
    def __init__(self, ctx_dim, mcm_dim):
        super(Diverter, self).__init__()
        self.linear_c = nn.Linear(ctx_dim, ctx_dim)
        self.linear_t = nn.Linear(ctx_dim, mcm_dim)

    def maxout(self, ctx):
        output = self.linear_c(ctx)
        output = torch.split(output, 2, -1)
        output = torch.stack(output,2)
        output = output.topk(k=1,dim=-1)[0]
        output = output.squeeze(-1)
        return output


    def forward(self, mcm, ctx):
        # mxl_c -> 1xmxl_c -> bxmxl_c
        b_s = ctx.size(0)
        n_m,l_m = mcm.size()
        mcm = mcm.unsqueeze(0).expand(b_s, n_m, l_m)
        # bx1xl_c
        # t = self.maxout(ctx)
        t = self.linear_c(ctx)
        # bx1xl_m x bxl_mxm -> bx1xm 
        t = self.linear_t(t)
        mcm = mcm.transpose(2,1)

        output = torch.bmm(t, mcm)

        return output