import time
import nmt.utils.misc_utils as utils
import torch
from torch.autograd import Variable
import random
import os
import sys
import math
import nmt
import torch.nn as nn
class Statistics(object):
    """
    Train/validate loss statistics.
    """
    def __init__(self, loss=0, n_words=0):
        self.loss = loss
        self.n_words = n_words
        self.start_time = time.time()

    def update(self, loss, n_words):
        self.loss += loss
        self.n_words += n_words

    def ppl(self):
        return utils.safe_exp(self.loss / self.n_words)
    def elapsed_time(self):
        return time.time() - self.start_time

    def print_out(self, epoch, batch, n_batches, loss):

        out_info = ("Epoch %2d, %5d/%5d| ppl: %6.2f| %4.0f s elapsed") % \
              (epoch, batch, n_batches,
                self.ppl(),
                self.elapsed_time())

        print(out_info)
        sys.stdout.flush()

    def log(self, prefix, summary_writer, step, **kwargs):

        for key in kwargs:
            summary_writer.add_scalar(prefix + '/' + key, kwargs[key],step)

class Trainer(object):
    def __init__(self, model, train_iter, valid_iter,
                 train_loss, valid_loss, optim, lr_scheduler,
                 shard_size=32):

        self.model = model
        self.train_iter = train_iter
        self.valid_iter = valid_iter
        self.train_loss = train_loss
        self.valid_loss = valid_loss
        self.optim = optim
        self.lr_scheduler = lr_scheduler

        self.shard_size = shard_size

        # Set model in training mode.
        self.model.train()       

        self.global_step = 0
        self.step_epoch = 0

    def update(self, batch, shard_size):
        self.model.zero_grad()
        src_inputs = batch.src[0]
        src_lengths = batch.src[1].tolist()
        tgt_inputs = batch.tgt[0][:-1]
        tgt_outputs = batch.tgt[0][1:]
        tgt_lengths = batch.tgt[1].tolist()

        outputs, mecm_pros = self.model(src_inputs,tgt_inputs,src_lengths)

        loss,n_words = self.train_loss.compute_loss(outputs, tgt_outputs.transpose(0, 1).contiguous(), tgt_lengths, mecm_pros)

        self.optim.step()
        return loss,n_words

    def train(self, epoch, report_func=None):
        """ Called for each epoch to train. """
        total_stats = Statistics()
        report_stats = Statistics()
         
        for step_batch,batch in enumerate(self.train_iter):
            self.global_step += 1
            loss,n_words= self.update(batch, self.shard_size)
            report_stats.update(loss,n_words)
            total_stats.update(loss,n_words)

            if report_func is not None:
                report_stats = report_func(self.global_step,
                        epoch, step_batch, len(self.train_iter),
                        total_stats.start_time, self.optim.lr, report_stats) 


        return total_stats           

    # def validate(self):
    #     self.model.eval()
    #     valid_stats = Statistics()

    #     for batch in self.valid_iter:

    #         src_inputs = batch.src[0]
    #         src_lengths = batch.src[1].tolist()
    #         tgt_inputs = batch.tgt


    #         outputs = self.model(src_inputs,tgt_inputs,src_lengths)

    #         stats = self.valid_loss.monolithic_compute_loss(batch, outputs)
    #         valid_stats.update(stats)        
    #     # Set model back to training mode.
    #     self.model.train()
    #     return valid_stats

    def save_per_epoch(self, epoch, out_dir):
        f = open(os.path.join(out_dir,'checkpoint'),'w')
        f.write('latest_checkpoint:checkpoint_epoch%d.pkl'%(epoch))
        f.close()
        self.model.save_checkpoint(epoch, 
                    os.path.join(out_dir,"checkpoint_epoch%d.pkl"%(epoch)))
        
        

    
    def load_checkpoint(self, filenmae):
        f = open(os.path.join(out_dir,'checkpoint'),'w')
        f.write('latest_checkpoint:checkpoint_epoch%d.pkl'%(epoch))
        f.close()

        self.load_checkpoint(filenmae)

        
        

    def epoch_step(self, epoch, out_dir):
        """ Called for each epoch to update learning rate. """
        # self.optim.updateLearningRate(ppl, epoch) 
        # self.lr_scheduler.step()
        # self.save_per_epoch(epoch, out_dir)
        self.drop_checkpoint(epoch,out_dir)


    def drop_checkpoint(self, epoch, out_dir):
        real_model = (self.model.module
                      if isinstance(self.model, nn.DataParallel)
                      else self.model)

        model_state_dict = real_model.state_dict()

        checkpoint = {
            'model': model_state_dict,
            'epoch': epoch,
        }
        torch.save(checkpoint,
                   os.path.join(out_dir,'checkpoint_epoch%d.pkl'
                   % (epoch)))

    def load_checkpoint(self, filenmae):
        ckpt = torch.load(filename)
        self.model.load_state_dict(ckpt['model'])
        epoch = ckpt['epoch']
        return epoch