import torch
from torch.autograd import Variable
import nmt
import torch.nn.functional as F
class Translator(object):
    def __init__(self, model, vocab, beam_size, max_length, replace_unk=True):
        self.model = model
        self.beam_size = beam_size
        self.replace_unk = replace_unk
        self.max_length = max_length
        self.vocab=vocab
        self.model.eval()
    def translate(self, src_input, src_input_lengths=None, mecm_idx=0, use_cuda=True):
        beam_size = self.beam_size
        encoder_outputs, encoder_hidden = self.model.encode(src_input, src_input_lengths, None)
        mecm_pros = self.model.diverter(self.model.mecm_params,encoder_hidden.transpose(0,1))
        mecm_pros = self.model.sm(mecm_pros)
        decoder_init_hidden = self.model.decoder.init_decoder_state(encoder_hidden)

        context_h = encoder_outputs
        batch_size = context_h.size(1)
        # Expand tensors for each beam.
        context = Variable(context_h.data.repeat(1, beam_size, 1))

        if not isinstance(decoder_init_hidden, tuple): # GRU
            dec_states = Variable(decoder_init_hidden.data.repeat(1, beam_size, 1))
     
        else: # LSTM
            dec_states = (
                Variable(decoder_init_hidden[0].data.repeat(1, beam_size, 1)),
                Variable(decoder_init_hidden[1].data.repeat(1, beam_size, 1)),
            )    

        beam = [
            nmt.Beam(beam_size, self.vocab, cuda=use_cuda)
            for k in range(batch_size)
        ]                


        dec_out = dec_states[0][-1].squeeze(0)


        batch_idx = list(range(batch_size))
        remaining_sents = batch_size

        for i in range(self.max_length):

            input = torch.stack(
                [b.get_current_state() for b in beam if not b.done]
            ).t().contiguous().view(1, -1)

            decoder_output, decoder_hidden = self.model.decode(
                Variable(input), 
                context, 
                dec_states,
                mecm_idx=mecm_idx
            )
            if not isinstance(decoder_init_hidden, tuple): # GRU
                dec_states = [
                    decoder_hidden
                ]                
            else:
                dec_states = [
                    decoder_hidden[0],
                    decoder_hidden[1]
                ]

            dec_out = decoder_output.squeeze(0)

            out = self.model.generator(dec_out).unsqueeze(0)


            word_lk = out.view(
                beam_size,
                remaining_sents,
                -1
            ).transpose(0, 1).contiguous()

            active = []
            for b in range(batch_size):
                if beam[b].done:
                    continue

                idx = batch_idx[b]
                if not beam[b].advance(word_lk.data[idx]):
                    active += [b]

                for dec_state in dec_states:  # iterate over h, c
                    # layers x beam*sent x dim
                    sent_states = dec_state.view(
                        -1, beam_size, remaining_sents, dec_state.size(2)
                    )[:, :, idx]
                    sent_states.data.copy_(
                        sent_states.data.index_select(
                            1,
                            beam[b].get_current_origin()
                        )
                    )

            if not active:
                break         

            # in this section, the sentences that are still active are
            # compacted so that the decoder is not run on completed sentences
            active_idx = torch.LongTensor([batch_idx[k] for k in active])
            if use_cuda:
                active_idx = active_idx.cuda()
            batch_idx = {beam: idx for idx, beam in enumerate(active)}                   


            def update_active(t):
                # select only the remaining active sentences
                view = t.data.view(
                    -1, remaining_sents,
                    self.model.decoder.hidden_size
                )
                new_size = list(t.size())
                new_size[-2] = new_size[-2] * len(active_idx) \
                    // remaining_sents
                return Variable(view.index_select(
                    1, active_idx
                ).view(*new_size))   

            if not isinstance(decoder_init_hidden, tuple): # GRU
                dec_states = update_active(dec_states[0])
                
            else:

                dec_states = (
                    update_active(dec_states[0]),
                    update_active(dec_states[1]),
                )
            dec_out = update_active(dec_out)
            context = update_active(context)

            remaining_sents = len(active)                         

        #  (4) package everything up

        allHyp, allScores = [], []
        n_best = 1

        for b in range(batch_size):
            scores, ks = beam[b].sort_best()

            allScores += [scores[:n_best]]
            hyps = zip(*[beam[b].get_hyp(k) for k in ks[:n_best]])
            allHyp += [hyps]

        return allHyp, allScores

