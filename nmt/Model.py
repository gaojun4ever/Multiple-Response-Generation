import torch
import torch.nn as nn
from torch.nn.parameter import Parameter
class MecmModel(nn.Module):
    def __init__(self, embedding_encoder, embedding_decoder, diverter, encoder, decoder, generator):
        super(MecmModel, self).__init__()
        self.embedding_encoder = embedding_encoder
        self.embedding_decoder = embedding_decoder
        self.encoder = encoder
        self.diverter = diverter
        # self.ench2dech = nn.Linear(2*decoder.hidden_size,decoder.hidden_size)
        self.decoder = decoder
        self.generator = generator
        self.mecm_params = Parameter(torch.randn(4,decoder.hidden_size),requires_grad=True)

        self.logsm = nn.LogSoftmax(-1)

    def forward(self, src_inputs, tgt_inputs, src_lengths):

        # Run wrods through encoder

        encoder_outputs, encoder_hidden = self.encode(src_inputs, src_lengths, None)
        mecm_logits = self.diverter(self.mecm_params,encoder_hidden.transpose(0,1))
        mecm_log_pros = self.logsm(mecm_logits)
        max_mecm_log_pros, max_mecm_idx= mecm_log_pros.max(-1)

        # batch_mecm = self.mecm_params.unsqueeze(0).expand(max_mecm_idx.size(0),
        #                                 self.mecm_params.size(0),
        #                                 self.mecm_params.size(1))
        batch_max_mecm = \
            torch.index_select(self.mecm_params, 0, max_mecm_idx.squeeze())
        
        # print(batch_max_mecm)
        max_mecm_log_pros = max_mecm_log_pros.squeeze()
        # print(max_mecm_log_pros)
        decoder_init_hidden = self.decoder.init_decoder_state(encoder_hidden)

        decoder_outputs , decoder_hiddens = self.decode(
                tgt_inputs, encoder_outputs, decoder_init_hidden, batch_max_mecm
            )        
        outputs = self.generator(decoder_outputs)
        # outputs = []
        # for i in range(self.mecm_params.size(0)):
            
        #     decoder_outputs , decoder_hiddens = self.decode(
        #             tgt_inputs, encoder_outputs, decoder_init_hidden, i
        #         )        
        #     outputs.append(self.generator(decoder_outputs))
        # outputs = decoder_outputs
        return outputs, max_mecm_log_pros



    def encode(self, input, lengths=None, hidden=None):
        emb = self.embedding_encoder(input)
        encoder_outputs, encoder_hidden = self.encoder(emb, lengths, None)

        return encoder_outputs, encoder_hidden

    def decode(self, input, context, state, mecm_param):
        # mecm_param = self.mecm_params[mecm_idx].unsqueeze(0).expand(input.size(1), self.mecm_params.size(1))
        mecm_param = mecm_param.unsqueeze(0)
        # last_hidden = torch.cat([mecm_param,state], -1) 
        # last_hidden = self.ench2dech(last_hidden)
        last_hidden = mecm_param+state
        emb = self.embedding_decoder(input)
        decoder_outputs , decoder_hiddens = self.decoder(
                emb, context, last_hidden
            )   
        return decoder_outputs, decoder_hiddens  
    def infer_decoder(self, input, context, state):

        emb = self.embedding_decoder(input)
        decoder_outputs , decoder_hiddens = self.decoder(
                emb, context, state
            )   
        return decoder_outputs, decoder_hiddens
        
    
    def save_checkpoint(self, epoch, filename):
        torch.save({'encoder_dict': self.encoder.state_dict(),
                    'decoder_dict': self.decoder.state_dict(),
                    'embedding_encoder_dict': self.embedding_encoder.state_dict(),
                    'embedding_decoder_dict': self.embedding_decoder.state_dict(),
                    'diverter_dict': self.diverter.state_dict(),
                    'generator_dict': self.generator.state_dict(),
                    'epoch': epoch,
                    },
                   filename)

    def load_checkpoint(self, filename):   
        ckpt = torch.load(filename)
        # self.embedding_encoder.load_state_dict(ckpt['embedding_encoder_dict'])
        # self.embedding_decoder.load_state_dict(ckpt['embedding_decoder_dict'])
        # self.encoder.load_state_dict(ckpt['encoder_dict'])
        # self.decoder.load_state_dict(ckpt['decoder_dict'])
        # self.diverter.load_state_dict(ckpt['diverter_dict'])
        # self.generator.load_state_dict(ckpt['generator_dict'])
        self.load_state_dict(ckpt['model'])
        epoch = ckpt['epoch']
        return epoch

