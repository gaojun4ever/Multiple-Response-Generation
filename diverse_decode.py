import torch
import argparse
import codecs
import nmt
from torch import cuda
from nmt.Diverse import Translator
def translate_sentence(translator, sent, fields, mecm_idx, use_cuda):
    src_input_var, src_input_lengths= \
        nmt.data_utils.batch_seq2var([sent],
                                    fields['src'].vocab.stoi,
                                    use_cuda)

    ret = translator.translate_batch(src_input_var,src_input_lengths,mecm_idx)

    return ret

def translate_file(translator, src_fin, tgt_fout, fields, mecm_idx, use_cuda):

    def get_sentence(sent_list):
        all_hyp_inds = sent_list#ret['predictions'][0]
        
        all_hyp_words = [nmt.data_utils.indices2words(idxs,fields['tgt'].vocab.itos) for idxs in all_hyp_inds]
        sentence_out = ' '.join(all_hyp_words[0])


        return sentence_out        

    print('start translating ...')
    with codecs.open(src_fin, 'r', encoding='utf8', errors='ignore') as src_file,\
            codecs.open(tgt_fout, 'wb', encoding='utf8') as tgt_file:
        src_lines = src_file.readlines()
        process_bar = nmt.misc_utils.ShowProcess(len(src_lines))
        for line in src_lines:
            src_seq = line.strip()
            ret = translate_sentence(translator, src_seq, fields, mecm_idx, use_cuda)
            sentence_out = get_sentence(ret['predictions'][0])
            tgt_file.write(sentence_out+'\n')

            process_bar.show_process()


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("-config", type=str, default="./config.yml")
    parser.add_argument("-src_in", type=str)
    parser.add_argument("-tgt_out", type=str)
    parser.add_argument("-model", type=str)
    parser.add_argument("-data", type=str)
    parser.add_argument("-mecm_idx", type=int)
    parser.add_argument("-beam_size", type=int)

    parser.add_argument('-gpuid', default=[], nargs='+', type=int)
    args = parser.parse_args()
    opt = nmt.misc_utils.load_hparams(args.config)

    use_cuda = False
    if args.gpuid:
        cuda.set_device(args.gpuid[0])
        use_cuda = True
    fields = nmt.IO.load_fields(
                torch.load(args.data + '.vocab.pkl'))

    model = nmt.model_helper.create_base_model(opt,len(fields['src'].vocab), 
                                            len(fields['tgt'].vocab), 
                                            fields['tgt'].vocab.stoi[nmt.IO.PAD_WORD])


    print('Loading parameters ...')

    model.load_checkpoint(args.model)
    if use_cuda:
        model = model.cuda()    

    translator = Translator(model, 
                        fields,
                        args.beam_size,
                        cuda=use_cuda)

    mecm_idx = args.mecm_idx
    print("translating file with mecm_idx %d"%(mecm_idx))
    translate_file(translator, args.src_in, args.tgt_out, fields, mecm_idx, use_cuda)
    

if __name__ == '__main__':
    main()